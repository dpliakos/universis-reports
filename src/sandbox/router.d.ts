import {ExpressDataApplication} from '@themost/express';
import {Router} from 'express';

export declare function sandboxRouter(app: ExpressDataApplication): Router;

import express from 'express';
import {HttpMethodNotAllowedError} from '@themost/common';
// eslint-disable-next-line no-unused-vars
import {ExpressDataApplication, serviceRouter} from '@themost/express';
import {SandboxContext} from "./sandboxContext";
// noinspection JSUnusedGlobalSymbols
/**
 * @param {ExpressDataApplication} app
 * @returns {Router}
 */
function sandboxRouter(app) {
    /**
     * @type {Router}
     */
    let router = express.Router();
    // noinspection DuplicatedCode
    router.use('/api', function (req, res, next) {
        // todo: use cors() for OPTIONS requests
        // disable any other method than GET
        if (req.method.toUpperCase() !== 'GET') {
            return next(new HttpMethodNotAllowedError('Method not allowed. Api server sandbox is readonly.'));
        }
        return next();
    }, function (req, res, next) {
        // create sandbox context
        const sandboxContext = new SandboxContext(app);
        /**
         * @type {DataContext|*}
         */
        const interactiveContext = req.context;
        // finalize already assigned context
        if (interactiveContext) {
            if (typeof interactiveContext.finalize === 'function') {
                return interactiveContext.finalize(() => {
                    // assign sandbox context
                    Object.defineProperty(req, 'context', {
                        enumerable: false,
                        configurable: true,
                        get: function() {
                            return sandboxContext;
                        }
                    });
                });
            }
        }
        // assign sandbox context to request
        Object.defineProperty(req, 'context', {
            enumerable: false,
            configurable: true,
            get: function() {
                return sandboxContext;
            }
        });
        // and return
        return next();
    }, serviceRouter);
    return router;
}
export {
    sandboxRouter
};

import {DefaultDataContext} from '@themost/data';
import {ExpressDataApplication} from '@themost/express';

export declare class SandboxContext extends DefaultDataContext {
    constructor(app: ExpressDataApplication);
}
